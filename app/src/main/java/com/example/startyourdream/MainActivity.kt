package com.example.startyourdream

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.Layout
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import kotlin.Exception


class MainActivity : AppCompatActivity() {
    private lateinit var mAuth: FirebaseAuth
    private val TAG = "Sign in Activity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        SignUptextView1.setOnClickListener { startActivity(Intent(applicationContext, RegistrationActivity::class.java))
        finish()}

        loginbutton.setOnClickListener {
            val name = LoginUserNameEditText.text.toString()
            val password = LoginPasswordEditText.text.toString()
            if (Patterns.EMAIL_ADDRESS.matcher(name).matches()) {
LoginUserNameEditText.setError("Invalide email ")
                    LoginUserNameEditText.isFocusable = true
                }
            else{
                loginUser(name, password)
            }
        }
        //forgot password
        forgotpasswordtextView.setOnClickListener { forgotPasswordDialog() }

    }

    private fun loginUser(name: String, password: String) {
 val mCustomToken:String
        mAuth.signInWithCustomToken(mCustomToken)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCustomToken:success")
                        val user = mAuth.currentUser
                       // updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCustomToken:failure", task.exception)
                        Toast.makeText(this@MainActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                      //  updateUI(null)
                    }
                }.addOnFailureListener { exception ->

                }
    }
    fun forgotPasswordDialog(){
      val builder=  AlertDialog.Builder(this)
        builder.setTitle("Recover Password")
        val positiveButtonClick = { dialog: DialogInterface, which: Int ->
            val Remailet  = EditText (this).toString().trim()
            beginRecovery(Remailet)
        }
        val cancelButtonClick = { dialog: DialogInterface, which: Int ->
           // val editText= EditText (this).toString().trim()
        dialog.dismiss()
        }

        val layout= LinearLayout(this)
        val editText= EditText (this)
        editText.hint = "Email"
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.minEms = 16
        layout.addView(editText)
        layout.setPadding(10,10,10,10)

        builder.setPositiveButton("Recover",DialogInterface.OnClickListener(positiveButtonClick))
        builder.setNegativeButton("Cancel",DialogInterface.OnClickListener(cancelButtonClick))
        builder.create().show()
builder.setView(layout)

        }

    private fun beginRecovery(Remailet: String) {
       mAuth.sendPasswordResetEmail(Remailet).addOnCompleteListener{task ->

         if(task.isSuccessful){
             Toast.makeText(this,"Email Sent",Toast.LENGTH_LONG).show()
         }
           else {
             Toast.makeText(this,"Failed To Send Email.....",Toast.LENGTH_LONG).show()
         }
        }   .addOnFailureListener { Exception ->
           Toast.makeText(this,"Failed Due To"+Exception.message,Toast.LENGTH_LONG).show()
       }

}

}