package com.example.startyourdream

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_registration.*


class RegistrationActivity : AppCompatActivity() {
    private val TAG = "CreateAccountActivity"
    private lateinit var  user:FirebaseUser
    private lateinit var mAuth: FirebaseAuth
    val mProgressDialog = ProgressDialog(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAuth = FirebaseAuth.getInstance()
        mProgressDialog.setTitle("Regitering")
        mProgressDialog.setMessage("waite a moment ")
        setContentView(R.layout.activity_registration)
        signupbutton.setOnClickListener {
            val email = RegisEmailEditText.toString().trim()
            val password = RegisPasswordEditText.toString()
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                RegisEmailEditText.setError("Invalid Email address")
                RegisEmailEditText.isFocusable = true
            } else if (password.length <= 8) {
                RegisPasswordEditText.setError("password length atleast 6 charect")
                RegisPasswordEditText.isFocusable = true
            } else { registerUser(email, password)
            }
        }
        haveanaccount.setOnClickListener {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }
    }

    private fun registerUser(email: String, password: String) {
        mAuth!!
                .createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    mProgressDialog!!.hide()

                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success")
                        user = mAuth.currentUser!!
                         mProgressDialog.show()
                        startActivity(Intent(applicationContext, ProfileActivity::class.java))
                        finish()
                    } else {
                        // If sign in fails, display a message to the user.

                        Toast.makeText(applicationContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        MyFailureListener()
                    }
                }

                                }
    fun MyFailureListener () {
        fun onFailure(exception: Exception) {
            val errorCode = (exception as FirebaseAuthException).errorCode
            val errorMessage = exception.message
            Toast.makeText(applicationContext, "Authentication failed."+errorMessage,
                    Toast.LENGTH_SHORT).show()
        }
    }
                }

//        override fun onStart() {
//            super.onStart()
//            // Check if user is signed in (non-null) and update UI accordingly.
//            val currentUser = mAuth.currentUser
//
//        }
//
//        override fun onSupportNavigateUp(): Boolean {
//            onBackPressed()
//            return super.onSupportNavigateUp()
//        }


